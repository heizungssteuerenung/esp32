#isntalling pacages
sudo apt-get install jq
pip install esptool
pip isntall ampy

#downloading files
curl -o esp32.bin -s https://micropython.org/resources/firmware/ESP32_GENERIC-20231005-v1.21.0.bin
curl -o boot.py -s https://gitlab.com/heizungssteuerenung/esp32/-/raw/main/src/boot.py?ref_type=heads&inline=false

#preparing credentials
sudo grep -hr '^ssid=' /etc/NetworkManager/system-connections/ > network.txt
sudo grep -hr '^psk=' /etc/NetworkManager/system-connections/ >> network.txt
hostname -i > ip.txt

#erase memory and flash chip
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32.bin
ampy -p /dev/ttyUSB0 put boot.py 
ampy -p /dev/ttyUSB0 put network.txt
ampy -p /dev/ttyUSB0 put ip.txt

#remove files
rm esp32.bin
rm network.txt
