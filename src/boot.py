import network
import requests
import time


ssid = "das_isch_anders"
psk = "imyourhotspot*"

def get_wificred():
    with open("network.txt", 'r') as datei:
        for zeile in datei:
            # Teile die Zeile anhand des Gleichheitszeichens auf
            teile = zeile.strip().split('=')
            if len(teile) == 2:
                # Prüfe, ob es sich um die Zeilen mit ssid und psk handelt
                if teile[0] == 'ssid':
                    ssid = teile[1]
                elif teile[0] == 'psk':
                    psk = teile[1]

        
def do_connect():

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ssid, psk)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())

def get_Main():
    URL = "https://gitlab.com/heizungssteuerenung/esp32/-/raw/main/src/main.py?ref_type=heads&inline=false"
    response = requests.get(URL)
    return response
    



#get_wificred()
time.sleep(1)
do_connect()
time.sleep(1)

#schleife,welche main alle 24 h akktualisiert
while True:

    open("main.py","wb").write(get_Main().content)
    time.sleep(1)
    import main as mainScript  
    time.sleep(1) 
    mainScript.main()


