import time
from umqtt.simple import MQTTClient
import random
import esp32

f = open("ip.txt","r")
Server = f.read()

def mqtt(data):
    c = MQTTClient(
        client_id = 0,
        server = Server,
    )
    c.connect()
    c.publish("temps/livingroom", b'{}'.format(data))
    c.disconnect()

def main():
    i =0
    while i<86400:
        time.sleep(1)
        temperature_value = (esp32.raw_temperature()-32)*5/9-20
        print(temperature_value)
        mqtt(temperature_value)

if __name__ == "__main__":
    main()


